#ifndef _Plane_h
#define _Plane_h
#include <iostream>
#include <list>

class Plane;
using namespace std;

class Buffer 
{
public:
	Buffer();
	~Buffer();
	void SetPlanes(list<Plane*> lis);

private:
	list<Plane*> planes;
};

#endif