#include "Plane.h"
#include<iostream>
#include<list>

using namespace std;

Plane::Plane(int row, int col, string t) {
	rowPos = row;
	colPos = col;
	type = t;
	string** planeArray = 0;
	coordinates = new int[2];
	coordinates[0] = row;
	coordinates[1] = col;
	history.emplace_back(coordinates);
	planeArray = new string*[8];
	for (int i = 0; i <= 7; i++)
	{
		planeArray[i] = new string[7];
		for (int j = 0; j <= 6; j++)
			if (row == i && col == j)
				planeArray[i][j] = t;
			else
				planeArray[i][j] = "0";
	}
	plane = planeArray;
}

Plane::~Plane()
{
}

void Plane::Move(int row, int column) 
{
	coordinates = new int[2];
	coordinates[0] = row;
	coordinates[1] = column;
	history.emplace_back(coordinates);
	string** planeArray = 0;
	planeArray = new string*[8];
	for (int i = 0; i <= 7; i++)
	{
		planeArray[i] = new string[7];
		for (int j = 0; j <= 6; j++)
			if (row == i && column == j)
				planeArray[i][j] = type;
			else
				planeArray[i][j] = "0";
	}
	plane = planeArray;
}

void Plane::Print() 
{
	for (int i = 0; i <= 7; i++)
	{
		for (int j = 0; j <= 6; j++)
			std::cout << plane[i][j] << ' ';
		std::cout << '\n';
	}
}

int* Plane::getCoordinates() 
{
	return coordinates;
}

string** Plane::getPlane() 
{
	return plane;
}

int Plane::getRow() 
{
	return rowPos;
}

int Plane::getCol() 
{
	return colPos;
}

list<int*> Plane::getHistory() 
{
	return history;
}

string** Plane::combinePlanes(list<Plane> lis) 
{
	//i want to make sure we agree to do this here
}