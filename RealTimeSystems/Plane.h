#ifndef _Plane_h
#define _Plane_h
#include <iostream>
#include <list>

using namespace std;

class Plane
{
public:
	Plane(int r, int c, string t);
	void Move(int row, int col);
	void Print();
	int* getCoordinates();
	string** getPlane();
	int getRow();
	int getCol();
	list<int*> getHistory();
	string** combinePlanes(list<Plane> lis);
	~Plane();

private:
	string type;
	int rowPos;
	int colPos;
	string** plane;
	int* coordinates;
	list<int*> history;
};
#endif